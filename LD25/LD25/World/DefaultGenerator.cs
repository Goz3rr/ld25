﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using LD25.Pandemic;

namespace LD25.World
{
    public class DefaultGenerator
    {
        int width;
        int height;
        float[][] noise;
        Random r;

        public DefaultGenerator(int width, int height)
        {
            this.width = width;
            this.height = height;
            r = new Random();
        }

        public Texture2D GenerateImage()
        {
            Texture2D output = new Texture2D(Main.graphicsDevice, width, height);
            Color[] data = new Color[width * height];
            output.GetData(data);

            noise = Noise.GeneratePerlinNoise(width, height, 10);
            int amountOfWater = 0;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int index = (int)(x + y * width);
                    data[index] = heightToColor(noise[x][y]);

                    if (noise[x][y] * 90f < 51)
                    {
                        //if(x < width-1)
                            //if(noise[x+1][y] < 50)
                                //data[index] = new Color(255, 0, 252); // Gets rid a few artefacts

                        amountOfWater++;
                    }
                }
            }

            int percent = (int)((float)amountOfWater / (width * height) * 100);
            if (percent > 70 || percent < 25) return GenerateImage();

            output.SetData(data);
            return output;
        }

        public List<City> GenerateCities()
        {
            List<City> output = new List<City>();
            if (noise == null) return output;

            for (int i = 0; i < r.Next(8,32); i++)
            {
                City city = new City();
                city.pos = getPosOnLand(output);
                city.size = r.Next(8, 32);
                city.name = Util.generateRandomCityName();
                output.Add(city);
            }

            return output;
        }

        public Texture2D GenerateRoads(Texture2D world, List<City> cities)
        {
            Texture2D output = new Texture2D(Main.graphicsDevice, width, height);
            Color[] data = new Color[width * height];

            /* unneeded
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    data[x + y * width] = Color.Transparent;
            */

            foreach (City city in cities)
            {
                int dist = 256;

                List<CityDist> cityDist = new List<CityDist>();
                foreach (City city2 in cities)
                    cityDist.Add(new CityDist(city2, (int)Vector2.Distance(city.pos, city2.pos)));

                cityDist.Sort(delegate(CityDist c1, CityDist c2) { return c1.dist.CompareTo(c2.dist); });

                while (city.connectedTo.Count < 2)
                {
                    foreach (CityDist cityD in cityDist)
                    {
                        City city2 = cityD.city;
                        int distance = cityD.dist;
                        if (city != city2 && !city.connectedTo.Contains(city2) && !city2.connectedTo.Contains(city) && distance < dist)
                        {
                            city.connectedTo.Add(city2);
                            foreach (City c in city2.connectedTo)
                                city.connectedTo.Add(c);

                            city2.connectedTo.Add(city);
                            foreach (City c in city.connectedTo)
                                city2.connectedTo.Add(c);

                            Vector2 curPos = city.pos;
                            bool done = false;
                            while (done == false)
                            {
                                int index = (int)(curPos.X + curPos.Y * width);
                                data[index] = Color.Black;

                                Vector2 dir = city2.pos - curPos;
                                int dirx = (int)(dir.X / Math.Abs(dir.X));
                                if (dir.X == 0) dirx = 0;

                                int diry = (int)(dir.Y / Math.Abs(dir.Y));
                                if (dir.Y == 0) diry = 0;

                                curPos += new Vector2(dirx, diry);

                                if (curPos == city2.pos)
                                    done = true;
                            }
                        }
                    }

                    dist += 16;
                }
            }

            foreach (City city in cities)
            {
                city.size = (int)MathHelper.Clamp(8 + city.connectedTo.Count,8,24);

                city.alivepeople = city.size * r.Next(10000, 15000);
            }

            output.SetData(data);
            return output;
        }

        private Vector2 getPosOnLand(List<City> cities)
        {
            if (noise == null) return new Vector2();

            Vector2 pos = new Vector2(r.Next(0, width), r.Next(0, height));
            float n = noise[(int)pos.X][(int)pos.Y] * 90f;
            if (n > 52 && n < 70 && pos.X > 16 && pos.X < Main.graphicsDevice.Viewport.Width - 16 && pos.Y > 16 && pos.Y < Main.graphicsDevice.Viewport.Height - 16)
            {
                foreach (City c in cities)
                {
                    if (Vector2.Distance(pos, c.pos) < 128)
                        return getPosOnLand(cities);
                }

                return pos;
            }
            else
                return getPosOnLand(cities);
        }

        public Color heightToColor(float height)
        {
            height = height * 90f;

            if (height < 35)
                return new Color(0, 0, 175);
            if (height < 40)
                return new Color(0, 0, 190);
            if (height < 46)
                return new Color(0, 0, 210);
            if (height < 48)
                return new Color(20, 20, 235);
            if (height < 50)
                return new Color(40, 40, 245);
            if (height < 51)
                return new Color(50, 50, 255);
            if (height < 52)
                return new Color(210, 210, 0);
            if (height < 53)
                return new Color(200, 200, 0);
            if (height < 54)
                return new Color(0, 190, 0);
            if (height < 58)
                return new Color(0, 180, 0);
            if (height < 62)
                return new Color(0,170,0);
            if (height < 64)
                return new Color(0, 160, 0);
            if (height < 66)
                return new Color(0, 150, 0);
            if (height < 68)
                return new Color(150, 100, 4);
            if (height < 70)
                return new Color(164,117,4);
            if (height < 72)
                return new Color(200, 200, 200);
            if (height < 75)
                return new Color(220, 220, 220);
            else
                return Color.White;

            /*
            if (height < 40)
                return Color.Blue;
            else if (height < 60)
                return Color.LightBlue;
            else if (height < 70)
                return Color.Beige;
            else if (height < 100)
                return Color.Green;
            else if (height < 120)
                return Color.SandyBrown;
            else if (height < 180)
                return Color.Brown;
            else
                return Color.Gray;
             */
        }
    }
}
