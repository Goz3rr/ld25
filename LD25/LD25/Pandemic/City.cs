﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD25.Pandemic
{
    public class City
    {
        public Vector2 pos;
        public int size;
        public int alivepeople;
        public int deadpeople;
        public int infectedpeople;
        public string name;
        public bool infected;
        public bool transportOpen = true;
        public List<City> connectedTo = new List<City>();
    }

    public class CityDist
    {
        public City city;
        public int dist;

        public CityDist(City city, int dist)
        {
            this.city = city;
            this.dist = dist;
        }
    }
}
