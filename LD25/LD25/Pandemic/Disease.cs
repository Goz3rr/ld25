﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LD25.Pandemic
{
    public enum DiseaseType
    {
        Bacteria,
        Virus,
        Parasite,
        BioWeapon
    }

    public class Disease
    {
        public string name;
        public string dna;
        public DiseaseType type;
        public float deadly;
        public float contagious = 0.1f;
        public bool detected;        
        public float hygiene;
        public float airborne;
        public float airborneRadius;
        public float infectAnimals;
        private Random r = new Random();

        public Disease(string name, string dna, DiseaseType type)
        {
            this.name = name;
            this.type = type;
            this.dna = dna;

            this.contagious += (float)(r.NextDouble() * 0.05);
            this.deadly = (float)(r.NextDouble() * 0.01);

            this.hygiene = (float)(r.NextDouble() * 0.2);
            this.infectAnimals = (float)(r.NextDouble() * 0.01);
        }

        public void Update(PandemicManager pandemicManager)
        {
            List<City> toInfect = new List<City>();
            foreach (City city in pandemicManager.infectedCities)
            {
                if (city.infectedpeople > 0)
                {
                    //Console.WriteLine(city.name + " is possible");

                    // Kill already infected
                    //double toDie = r.NextDouble() * deadly;
                    int dieingInfected = 0;//(int)Math.Floor(city.infectedpeople * toDie);
                    for (int i = 0; i < city.infectedpeople; i++)                    
                        if (r.NextDouble() < deadly)
                            dieingInfected++;

                    if (dieingInfected >= city.infectedpeople) 
                    {
                        pandemicManager.newsTicker.addItem(new NewsItem(city.name + " has been exterminated!", Color.Red));
                        dieingInfected = city.infectedpeople;
                    }
                    pandemicManager.totalInfected -= dieingInfected;
                    pandemicManager.totalDeath += dieingInfected;
                    city.infectedpeople -= dieingInfected;
                    city.deadpeople += dieingInfected;
                    pandemicManager.score += dieingInfected;
                    pandemicManager.dnapoints += dieingInfected / 10000;

                    // See if we can spread to another city
                    foreach (City connnectedCity in city.connectedTo)
                    {
                        if (connnectedCity.transportOpen && city.transportOpen && city.infectedpeople > 0 && connnectedCity.infected == false)
                        {
                            float chanceSpread = (float)(((city.size + connnectedCity.size) / 16384f) * r.NextDouble());
                            float possibleSpread = city.infectedpeople * chanceSpread;

                            if (possibleSpread > 1)
                            {
                                connnectedCity.infected = true;
                                connnectedCity.infectedpeople += 1;
                                pandemicManager.totalInfected += 1;
                                pandemicManager.score += 100;
                                pandemicManager.dnapoints += connnectedCity.size/2;
                                toInfect.Add(connnectedCity);

                                pandemicManager.newsTicker.addItem(new NewsItem(connnectedCity.name + " was infected from " + city.name + "!", Color.Red));
                            }
                        }
                    }
                }

                if(city.alivepeople > 0)
                {
                    // Try to infect new people
                    //int possibleInfections = city.infectedpeople < 10 ? r.Next(0, 2) : (int)(city.infectedpeople * contagious);
                    double bonus = detected ? r.NextDouble() * 0.1f : 0;
                    int actualInfections = 0;//(int)Math.Floor(possibleInfections * (1 - (hygiene + bonus)));
                    for (int i = 0; i < city.infectedpeople; i++)
                        if (r.NextDouble() < contagious * (1 - (hygiene + bonus)))
                            actualInfections++;

                    if (actualInfections >= city.alivepeople)
                    {
                        pandemicManager.newsTicker.addItem(new NewsItem(city.name + " has been completely infected!", Color.Red));
                        actualInfections = city.alivepeople;
                    }
                    pandemicManager.totalInfected += actualInfections;
                    city.infectedpeople += actualInfections;
                    city.alivepeople -= actualInfections;
                    pandemicManager.score += actualInfections;
                    pandemicManager.dnapoints += actualInfections / 10000;
                }
            }

            if (airborne > 0)
            {
                //pandemicManager.PandemicSpread
                airborneRadius += airborne;

                foreach (City city in pandemicManager.cities)
                {
                    if (city == pandemicManager.startCity) continue;
                    if (city.infected == true) continue;
                    if (city.alivepeople <= 0) continue;

                    if (r.NextDouble() < airborne && Vector2.Distance(pandemicManager.startCity.pos, city.pos) < airborneRadius)
                    {                        
                        city.infected = true;
                        city.infectedpeople++;
                        city.alivepeople--;
                        pandemicManager.totalInfected += 1;
                        pandemicManager.score += 100;
                        pandemicManager.dnapoints += city.size / 2;
                        toInfect.Add(city);
                        pandemicManager.newsTicker.addItem(new NewsItem(city.name + " was infected from the airborne disease!", Color.Red));                                                                                        
                    }
                }
            }

            if (infectAnimals > 0)
            {

            }

            foreach (City city in toInfect)
                pandemicManager.infectedCities.Add(city);
            
        }
    }
}
