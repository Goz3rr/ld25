﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using LD25.Gui;

namespace LD25.Pandemic
{
    public class PandemicManager
    {
        public DateTime date;
        public Texture2D PandemicSpread;
        public string objective;
        public bool paused = false;
        public IGui currentGui;
        public Disease disease;
        public City startCity;
        public List<City> infectedCities;
        public List<City> cities;
        public NewsTicker newsTicker;
        public int score;
        public int totalDeath;
        public int totalInfected;
        public float dnapoints = 0;

        public PandemicManager(NewsTicker newsTicker)
        {
            this.newsTicker = newsTicker;

            infectedCities = new List<City>();

            date = DateTime.Today;            
            objective = "Create your disease!";

            currentGui = new CreateDiseaseMenu(this);
        }

        TimeSpan elapsedtime = TimeSpan.Zero;
        public void Update(GameTime gameTime)
        {
            if (!paused)
            {
                elapsedtime += gameTime.ElapsedGameTime;

                if (elapsedtime > TimeSpan.FromSeconds(1))
                {
                    elapsedtime = TimeSpan.Zero;
                    date = date.AddDays(1);

                    disease.Update(this);

                    if (Util.addRandomNews()==true) newsTicker.addItem(new NewsItem(Util.generateRandomNews()));
                }
            }

            if (currentGui != null)
                currentGui.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (disease != null && disease.airborneRadius > 0)
                spriteBatch.Draw(PandemicSpread, new Rectangle((int)startCity.pos.X, (int)startCity.pos.Y, (int)disease.airborneRadius, (int)disease.airborneRadius), null, Color.White * 0.5f, 0f, new Vector2(32, 32), SpriteEffects.None, 0);

            if(currentGui != null)
                currentGui.Draw(gameTime, spriteBatch);
        }

        public string getDate()
        {
            if(disease == null)
                return "Date: " + date.GetDateTimeFormats('d')[0];
            else
                return disease.name + " - Date: " + date.GetDateTimeFormats('d')[0];
        }
    }
}
