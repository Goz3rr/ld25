﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD25.Pandemic
{
    public class NewsItem
    {
        public string text;
        public Color color;
        public int showTimes;
        public int scroll;

        public NewsItem(string text, Color color, int showTimes = 3)
        {            
            this.text = text;
            this.color = color;
            this.showTimes = showTimes;
            this.scroll = 0;
        }

        public NewsItem(string text)
        {
            this.text = text;
            this.color = Color.White;
            this.showTimes = 3;
            this.scroll = 0;
        }

        public string show()
        {
            showTimes--;
            return text;
        }
    }

    public class NewsTicker
    {
        public List<NewsItem> items;

        public NewsTicker()
        {
            items = new List<NewsItem>();
        }

        public void addItem(NewsItem item)
        {
            items.Add(item);
        }

        int scroll = 0;
        public void Update(GameTime gameTime)
        {
            if(items.Count > 0)
                scroll++;
        }

        public void Draw(GameTime gameTime)
        {
            if (items.Count == 0) return;

            int drawoffset = 0;
            int count = 0;
            List<NewsItem> toRemove = new List<NewsItem>();
            //while (drawoffset < Main.graphicsDevice.Viewport.Width)
            {

                foreach (NewsItem item in items)
                {
                    if (drawoffset > Main.graphicsDevice.Viewport.Width) continue;

                    Main.spriteBatch.DrawString(Main.font, item.show(), new Vector2(Main.graphicsDevice.Viewport.Width - scroll + drawoffset, Main.graphicsDevice.Viewport.Height - 15), item.color);
                    drawoffset += (int)Main.font.MeasureString(item.text).X;

                    if (item.showTimes <= 0)
                        toRemove.Add(item);

                    if (count < items.Count-1)
                    {
                        Main.spriteBatch.DrawString(Main.font, " - ", new Vector2(Main.graphicsDevice.Viewport.Width - scroll + drawoffset, Main.graphicsDevice.Viewport.Height - 15), Color.White);
                        drawoffset += (int)Main.font.MeasureString(" - ").X;
                    }

                    count++;
                }
            }

            if (scroll >= Main.graphicsDevice.Viewport.Width + drawoffset)
            {
                scroll = 0;

                foreach (NewsItem item in toRemove)
                    items.Remove(item);
            }
        }
    }
}
