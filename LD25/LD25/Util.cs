﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using LD25.Pandemic;

namespace LD25
{
    public class Util
    {
        private static Random r = new Random();

        public static void DrawShadowText(SpriteBatch spriteBatch, SpriteFont font, String text, Vector2 pos, Color color)
        {
            spriteBatch.DrawString(font, text, pos + new Vector2(1,1), Color.Black);
            spriteBatch.DrawString(font, text, pos, color);
        }

        public static bool XYInsideRect(int x, int y, Rectangle rect)
        {
            return rect.Contains(x, y);
        }

        public static bool addRandomNews()
        {
            return (r.NextDouble() < 0.01);
        }

        public static string generateRandomNews()
        {
            string[] newsItems = { "Martians invade earth", "New Security Exploit In Microsoft Internet Explorer", "World's First Gameboy Advance Beowulf Cluster", "Microsoft Stopping Development On DOS 6.22", 
                                   "Goats are the new cats", "Intel Makes A Breakthrough In Homemade Rockets", "Yahoo Has Cloned Spider-man"};

            return newsItems[r.Next(0, newsItems.Length)];
        }

        public static DiseaseType generateRandomType()
        {
            Array arr = Enum.GetValues(typeof(DiseaseType));
            return (DiseaseType)arr.GetValue(r.Next(0, arr.Length));
        }

        public static string generateRandomDNA()
        {
            string output = "";
            for (int i = 0; i < 8; i++)
            {
                if (r.Next(0, 10) < 5)
                {
                    if(r.Next(0, 10) < 5)
                        output += "AT";
                    else
                        output += "TA";
                }
                else
                {
                    if (r.Next(0, 10) < 5)
                        output += "GC";
                    else
                        output += "CG";
                }
            }

            return output;
        }

        public static string generateRandomDiseaseName()
        {
            string[] prefix = { "Mad goat ", "huskitch", "Huskitchsons ", "Virusek's ", "virus", "meta", "abdomino", "adreno", "aero", "ambi", "ana",
                                "balano", "bi", "bio", "blepharo", "brachio", "bronch", "bronchi", "capit", "cardio", "cata", "cerebello", 
                                "cerbero", "chole", "chromato", "circum", "colo", "contra", "cycl", "cyano", "dermato", "dermo", "di", 
                                "dys", "digit", "ec", "epi", "eu", "ex", "exo", "extra", "fibro", "filli", "genu", "glauco", "gono",
                                "halluc", "hemat", "hema", "hepat", "hydro", "homo", "hyper", "infra", "inter", "ipsi", "iso", "kerato",
                                "lepto", "leuk", "lipo", "macro", "mammo", "melos", "mero", "meso", "metro", "micro", "narco", "nerv", "neo",
                                "necro", "neuro", "normo", "poly", "pre", "post"};

            string[] suffix = { " Disease", "acal", "acusis", "aemia", "asthenia", "cele", "centesis", "cidal", "cide", "col", "cyte",
                                "dynia", "eal", "ectasia", "ectasis", "ectomy", "emesis", "emia", "form", "iform", "gen", "genic", 
                                "gnosis", "grapho", "gram", "iasis", "ic", "icle", "idio", "ist", "ite", "itis", "ium", "kine", "lepsis",
                                "lepsy", "logy", "lysis", "ptosis", "ptysis"};

            string first = prefix[(int)r.Next(0, prefix.Length)];
            string last = suffix[(int)r.Next(0, suffix.Length)];

            char firstEnd = first[first.Length - 1];
            char lastStart = last[0];

            if (firstEnd == ' ' && lastStart != ' ')
            {
                while (lastStart != ' ')
                {
                    last = suffix[(int)r.Next(0, suffix.Length)];
                    lastStart = last[0];
                }
            }

            if (first == last) return generateRandomDiseaseName();

            return (first + last).Replace("  "," ");
        }

        public static string generateRandomCityName()
        {
            string[] names = { 
            "Abhrante", "Celew", "Fiacus", "Lorix", "Sanberthges", "Aetarka", "Celey", "Fimbery", 
            "Luctistian", "Sanbor", "Ahel", "Celosse", "Findovech", "Lugh", "Sarthaid", "Ailbour", 
            "Cenwy", "Forg", "Luthis", "Satalis", "Alabaeta", "Ceole-Doman", "Fostavins", "Lyndere", 
            "Scea", "Alaechrim", "Cereitholl", "Froise", "Madir", "Scen", "Aleigh", "Ceremm", "Froyse", 
            "Maeva", "Schymarch", "Alfon", "Cerrethui", "Fynge", "Mair", "Seam", "Alkell", "Cestraith", 
            "Gaelm", "Maleen", "Semenus", "Allet", "Charr", "Gail", "Mamna", "Sengy", "Alley", "Chermie", 
            "Gair", "Mandelm", "Senthimedo", "Almansin", "Chlaisl", "Gallman", "Marsha", "Seva", 
            "Alwalteneth", "Chroich", "Garcan", "Maten", "Sewodeffan", "Alyn", "Ciarth", "Geswitire", 
            "Melinas", "Sexan", "Alys", "Cirdis", "Geua", "Meniell", "Shaaoll", "Ambara", "Clait", 
            "Gibul", "Merespa", "Sheen", "Amysel", "Clarik", "Gilnodahus", "Mhinks", "Shifiet", "Anaitli", 
            "Clarin", "Glarden", "Michisoth", "Shir", "Andred", "Cliog", "Gloiresthel", "Mild", "Sibill", 
            "Aneuron", "Compresa", "Godinc", "Mili", "Siego", "Angri", "Connet", "Goibhe", "Mill", "Sigeric", 
            "Angrimaydny", "Connontinus", "Goilfrak", "Milyssh", "Sikkeney", "Annen", "Connth", "Gonuadna", 
            "Minklow", "Silifllwynn", "Appo", "Coopellach", "Grace", "Mireadhrain", "Simoll", "Araglynn", 
            "Corocca", "Griseld", "Moire", "Sipreborn", "Argaledd", "Creburtyn", "Guores", "Morgia", 
            "Sireagorn", "Arhenn", "Cred", "Gwendic", "Morgis", "Skethner", "Arleeshald", "Cristhror", 
            "Gwlad", "Morthiri", "Slorn", "Arlonius", "Croula", "Gwyth", "Mosine", "Slow", "Arna", 
            "Crowhrim", "Hafpor", "Muirging", "Somerwan", "Arthador", "Cuerlinsras", "Hantane", "Muirine", 
            "Sopoldis", "Arvelte", "Cullyann", "Harad", "Mund", "Sopoliana", "Asceline", "Cuma", "Haran", 
            "Murvann", "Sorotharlim", "Asfaladrica", "Dabaylach", "Harap", "Naranithar", "Spin", "Ashlic", 
            "Dagonuala", "Harett", "Naugliff", "Stalchwn", "Asoran", "Death", "Heldeberid", "Naugwynlo", "Stil", 
            "Atwolskear", "Debridge", "Helsuil", "Nibhna", "Sulia", "Aximonith", "Deidhell", "Higinog", "Nimrodra", 
            "Sulie", "Ayron", "Deignessa", "Himus", "Nizd", "Sulwarikkin", "Bacian", "Deiregall", "Hithink", "Nolan", 
            "Svell", "Baith", "Delvynn", "Hohemmon", "Nualla", "Sween", "Baladet", "Dereux", "Hronnitsan", "Olinus", 
            "Sweet", "Banchaelos", "Dermacphe", "Hunbergessa", "Opermenely", "Swidith", "Banuiallisa", "Deverne", 
            "Hunbrian", "Orascow", "Talien", "Barsonton", "Diador", "Hwaeret", "Oren", "Taradyn", "Basaire", "Dice", 
            "Hwainmath", "Orenegenui", "Tean", "Beir", "Diet", "Iainsley", "Oret", "Telyn", "Bere", "Dithsing", 
            "Iarcharna", "Orga", "Teorg", "Beremabe", "Dolan", "Idmon", "Osgobnat", "Terle", "Berf", "Dolaz", 
            "Idolb", "Oskal", "Terna", "Berg", "Donika", "Ilmann", "Osmayer", "Tessir", "Bergarr", "Dorwfullind", 
            "Ima'In", "Osveighne", "Tessris", "Berlin", "Down", "Imaximonto", "Owinepth", "Teudegan", "Bern", 
            "Draide", "Imir", "Owunni", "Tewaynaldon", "Bers", "Dresruthui", "Inch", "Padel", "Theopold", "Bert", 
            "Drugbor", "Inchondor", "Padrin", "Thera", "Berth", "Drusilina", "Inius", "Paid", "Theri", "Berts", 
            "Dudd", "Ipswinn", "Pawls", "Thert", "Berty", "Dumnord", "Isee", "Penez", "Thinkinog", "Berz", "Dunedaiglen", 
            "Isel", "Perca", "Tholainn", "Bethir", "Dunedd", "Iset", "Pere", "Thra", "Bhuilos", "Dyler", "Ithor", "Perfyddolen", 
            "Thri", "Biddian", "Eadmund", "Iwand", "Perilswiky", "Tiartlakur", "Bius", "Ebeuzed", "Jefchil", "Perital", "Tothe", 
            "Bling", "Eblagh", "Jena", "Pert", "Tour", "Bobd", "Ecil", "Jene", "Pertanys", "Tous", "Brayne", "Ectath", "Jesuger", 
            "Pior", "Trich", "Bre-Rost", "Edie", "Jilian", "Pith", "Trick", "Brenth", "Edig", "Joce", "Plouara'In", "Trictiger", 
            "Brentz", "Edil", "Johandyn", "Pric", "Tryggur", "Bricca", "Edown", "Jokuli", "Prin", "Tumladys", "Britona", "Egon", 
            "Joss", "Prudet", "Twaldenna", "Broar", "Einglor", "Jownahad", "Quodlaculla", "Ungot", "Bromyr", "Elegouena", "Joyce", 
            "Qveneset", "Unwardobail", "Buccus", "Eleriador", "Just", "Reina", "Valane", "Burc", "Eliudh", "Kadaoila", "Reith", 
            "Valduil", "Burg", "Emildaz", "Kelsecga", "Rekweinia", "Valennig", "Bury", "Emiode", "Keryuna", "Remma", "Valewisa", 
            "Caenildorel", "Enth", "Kevyn", "Reo-Derrse", "Valoar", "Cala", "Entherman", "Khink", "Reogan", "Veltass", "Calbeirin", 
            "Entien", "Kimburgil", "Reps", "Vered", "Cald", "Eozef", "Labus", "Restuach", "Verell", "Caldaroll", "Ericanios", 
            "Laingwayneg", "Rhold", "Wain", "Call", "Eris", "Lamdris", "Rich", "Wallichfion", "Calonann", "Ernil", "Launir", "Ridget", 
            "Watta", "Calyn", "Etnote", "Lawleighte", "Rienthe", "Weser", "Carnacha", "Etriskaftin", "Legai", "Riffa", "Wheastiny", 
            "Carog", "Evel", "Leinoediva", "Rikthmain", "Wilhere", "Carol", "Ewborbes", "Lesha-Carta", "Rinman", "Wisinketith", "Caron", 
            "Eynthor", "Lhara", "Riwalle", "Wulf", "Cassen", "Falasus", "Libessie", "Rohie", "Xippa", "Cathian", "Falli", "Lien", 
            "Rohir", "Xulf", "Catweargan", "Fannmharian", "Lilisansink", "Rohirithal", "Yamaby", "Cean", "Favonelvon", "Lindlan", 
            "Roraldi", "Yenelly", "Cechta", "Fela", "Lithervorn", "Rozenny", "Ysmene", "Ceitha", "Felt", "Lomner", "Rudonaytes", 
            "Zenianthes", "Celdjananna", "Fene", "Loricus", "Saebba", "Zeustanwg", "Celeb", "Fennial", "Lorid", "Samm", "Zimiluciot", 
            "Celeg", "Ferguint", "Lorie", "Sammacke", "Zwel", "Celen", "Ffin", "Lorio", "Sammeis", "Zywinnef", "Jönköping", "Midgar" };

            return names[(int)r.Next(0, names.Length)];
        }
    }
}
