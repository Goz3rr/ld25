using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD25
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        public static Game game;
        public static GraphicsDeviceManager graphics;
        public static GraphicsDevice graphicsDevice;
        public static StateManager stateManager;

        public static PlayState playState = new PlayState();

        public static SpriteBatch spriteBatch;
        public static SpriteFont font;
        public static SpriteFont font_big;
        public static Texture2D whiteTexture;

        int framerate = 0;
        int framecounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;

        public Main()
        {
            game = this;
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferMultiSampling = true;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;                     
            graphics.ApplyChanges();

            this.IsMouseVisible = true;
            this.Window.Title = "LD25: You are the villain";

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphicsDevice = GraphicsDevice;
            spriteBatch = new SpriteBatch(GraphicsDevice);

            stateManager = new StateManager(Content);
            stateManager.LoadScreen(playState);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            font = Content.Load<SpriteFont>("font");
            font_big = Content.Load<SpriteFont>("font_big");

            whiteTexture = new Texture2D(GraphicsDevice, 1, 1);
            whiteTexture.SetData(new Color[] { Color.White });

            stateManager.LoadContent();
        }

        protected override void UnloadContent()
        {
            stateManager.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState K = Keyboard.GetState();

            elapsedTime += gameTime.ElapsedGameTime;
            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                framerate = framecounter;
                framecounter = 0;
            }

            if (K.IsKeyDown(Keys.Escape)) this.Exit();

            stateManager.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            framecounter++;
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            stateManager.Draw(gameTime);

            Util.DrawShadowText(spriteBatch, font, "FPS: " + framerate, new Vector2(2, 1), Color.White);   
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
