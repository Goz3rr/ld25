﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LD25.Pandemic;
using LD25.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LD25.Gui
{
    public class UpgradeDiseaseMenu : IGui
    {
        PandemicManager pandemicManager;
        bool visible;
        bool click = false;

        public UpgradeDiseaseMenu(PandemicManager pandemicManager)
        {
            this.pandemicManager = pandemicManager;
            Show();
        }

        public void Update(GameTime gameTime)
        {
            if (!Main.game.IsActive) return;
            if (pandemicManager.startCity == null) return;
            MouseState ms = Mouse.GetState();

            if (ms.LeftButton == ButtonState.Released) click = false;

            if (ms.LeftButton == ButtonState.Pressed && click == false)
            {
                if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 150, 120, 40))) // deadly
                {
                    int level = (int)(pandemicManager.disease.deadly * 10);
                    int points = (level + 1) * 10;

                    if (pandemicManager.dnapoints >= points && pandemicManager.disease.deadly < 1)
                    {
                        pandemicManager.dnapoints -= points;
                        pandemicManager.disease.deadly += 0.1f;
                    }
                }

                if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 200, 120, 40))) // airborne
                {
                    int level = (int)(pandemicManager.disease.airborne * 10);
                    int points = (level + 1) * 10;

                    if (pandemicManager.dnapoints >= points && pandemicManager.disease.airborne < 1)
                    {
                        pandemicManager.dnapoints -= points;
                        pandemicManager.disease.airborne += 0.1f;
                    }
                }

                if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 150, 120, 40))) // contagious
                {
                    int level = (int)(pandemicManager.disease.contagious * 10);
                    int points = (level + 1) * 10;

                    if (pandemicManager.dnapoints >= points && pandemicManager.disease.contagious < 1)
                    {
                        pandemicManager.dnapoints -= points;
                        pandemicManager.disease.contagious += 0.1f;
                    }
                }

                if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 200, 120, 40))) // animal
                {
                    return;

                    int level = (int)(pandemicManager.disease.infectAnimals * 10);
                    int points = (level + 1) * 10;

                    if (pandemicManager.dnapoints >= points && pandemicManager.disease.infectAnimals < 1)
                    {
                        pandemicManager.dnapoints -= points;
                        pandemicManager.disease.infectAnimals += 0.1f;
                    }
                }

                if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 250, 260, 40))) // done
                {
                    Close();
                }

                click = true;
            }

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!visible) return;

            int width = 300;
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - (width / 2), 100, width, 200), Color.Gray);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - (width / 2 - 1), 101, width - 2, 200 - 2), Color.DarkGray);

            spriteBatch.DrawString(Main.font_big, "Upgrade-o-tron", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("Upgrade-o-tron").X / 2, 95), Color.Black);

            int level = (int)(pandemicManager.disease.deadly * 10);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 150, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font, "deadly lvl" + level, new Vector2(Main.graphicsDevice.Viewport.Width / 2 + 70 - (int)Main.font.MeasureString("deadly lvl" + level).X / 2, 158), Color.Black);
            spriteBatch.DrawString(Main.font, (level + 1) * 10 + " DNA Points", new Vector2(Main.graphicsDevice.Viewport.Width / 2 + 70 - (int)Main.font.MeasureString((level + 1) * 10 + " DNA Points").X / 2, 174), Color.Black);

            level = (int)(pandemicManager.disease.airborne * 10);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 200, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font, "airborne lvl" + level, new Vector2(Main.graphicsDevice.Viewport.Width / 2 + 70 - (int)Main.font.MeasureString("airborne lvl" + level).X / 2, 208), Color.Black);
            spriteBatch.DrawString(Main.font, (level + 1) * 10 + " DNA Points", new Vector2(Main.graphicsDevice.Viewport.Width / 2 + 70 - (int)Main.font.MeasureString((level + 1) * 10 + " DNA Points").X / 2, 224), Color.Black);

            level = (int)(pandemicManager.disease.contagious * 10);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 150, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font, "contagious lvl" + level, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - 70 - (int)Main.font.MeasureString("contagious lvl" + level).X / 2, 158), Color.Black);
            spriteBatch.DrawString(Main.font, (level + 1) * 10 + " DNA Points", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - 70 - (int)Main.font.MeasureString((level + 1) * 10 + " DNA Points").X / 2, 174), Color.Black);

            level = (int)(pandemicManager.disease.infectAnimals * 10);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 200, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font, "animal lvl" + level, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - 70 - (int)Main.font.MeasureString("animal lvl" + level).X / 2, 208), new Color(100, 100, 100));
            spriteBatch.DrawString(Main.font, (level + 1) * 10 + " DNA Points", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - 70 - (int)Main.font.MeasureString((level + 1) * 10 + " DNA Points").X / 2, 224), new Color(100, 100, 100));

            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 250, 260, 40), Color.Gray);
            spriteBatch.DrawString(Main.font_big, "Done", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("Done").X / 2, 245), Color.Black);

        }

        public void Show()
        {
            pandemicManager.paused = true;
            visible = true;
        }

        public void Close()
        {
            pandemicManager.paused = false;
            pandemicManager.currentGui = null;
            visible = false;
        }
    }
}
