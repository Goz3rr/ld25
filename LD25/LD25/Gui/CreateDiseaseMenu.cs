﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LD25.Pandemic;
using LD25.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LD25.Gui
{
    public class CreateDiseaseMenu : IGui
    {
        PandemicManager pandemicManager;
        bool visible;

        string name, dna;
        DiseaseType type;

        public CreateDiseaseMenu(PandemicManager pandemicManager)
        {
            this.pandemicManager = pandemicManager;
            name = Util.generateRandomDiseaseName();
            dna = Util.generateRandomDNA();
            type = Util.generateRandomType();
            Show();
        }

        public void Update(GameTime gameTime)
        {
            if (!Main.game.IsActive) return;

            MouseState ms = Mouse.GetState();

            if(Util.XYInsideRect(ms.X, ms.Y-16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130,300,120,40))) // Regen
            {
                if (ms.LeftButton == ButtonState.Pressed)
                {
                    name = Util.generateRandomDiseaseName();
                    dna = Util.generateRandomDNA();
                    type = Util.generateRandomType();
                }
            }

            if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 300, 120, 40))) // Accept
            {
                if (ms.LeftButton == ButtonState.Pressed)
                {
                    pandemicManager.objective = "Select a city to start your pandemic!";
                    pandemicManager.disease = new Disease(name, dna, type);
                    Close();
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!visible) return;

            int width = 300;
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - (width/2), 100, width, 250), Color.Gray);
            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - (width/2-1), 101, width-2, 250-2), Color.DarkGray);

            //fuckingSpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone);
            spriteBatch.DrawString(Main.font_big, "Disease-o-matic", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("Disease-o-matic").X /2, 95), Color.Black);

            spriteBatch.DrawString(Main.font, "Name: " + name, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - (width/2) + 20, 150), Color.Black);
            spriteBatch.DrawString(Main.font, "Type: " + type, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - (width / 2) + 20, 165), Color.Black);
            spriteBatch.DrawString(Main.font, "DNA: " + dna, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - (width / 2) + 20, 180), Color.Black);

            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 250, 260, 40), Color.Gray);
            spriteBatch.DrawString(Main.font_big, "Customize", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("Customize").X / 2, 245), new Color(100,100,100));

            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 - 130, 300, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font_big, "Random", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - 70 - Main.font_big.MeasureString("Random").X / 2, 295), Color.Black);

            spriteBatch.Draw(Main.whiteTexture, new Rectangle(Main.graphicsDevice.Viewport.Width / 2 + 10, 300, 120, 40), Color.Gray);
            spriteBatch.DrawString(Main.font_big, "Accept", new Vector2(Main.graphicsDevice.Viewport.Width / 2 + 70 - Main.font_big.MeasureString("Accept").X / 2, 295), Color.Black);
        }

        public void Show()
        {
            pandemicManager.paused = true;
            visible = true;
        }

        public void Close()
        {
            //pandemicManager.paused = false;
            visible = false;
            pandemicManager.currentGui = null;
        }
    }
}
