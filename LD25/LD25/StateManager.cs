﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace LD25
{
    public interface IGameState
    {
        int drawLayer { get; set; }
        bool hasControl { get; set; }
        bool contentLoaded { get; set; }

        void Init();
        void LoadContent(ContentManager content);
        void UnloadContent();
        void Update(GameTime gameTime);
        void Draw(GameTime gameTime);
    }

    public class StateManager
    {
        public List<IGameState> states = new List<IGameState>();
        ContentManager content;
        bool contentLoaded = false;

        public StateManager(ContentManager content)
        {
            this.content = content;
        }

        public void LoadScreen(IGameState state)
        {
            state.drawLayer = 1;
            state.hasControl = true;

            states.Add(state);
            states.Sort(delegate(IGameState g1, IGameState g2) { return g1.drawLayer.CompareTo(g2.drawLayer); });
            state.Init();

            if (!state.contentLoaded && contentLoaded == true)
                state.LoadContent(content);
        }

        public bool UnloadScreen(IGameState state)
        {
            state.UnloadContent();
            return states.Remove(state);
        }

        public void LoadContent()
        {
            foreach (IGameState state in states)
            {
                if (!state.contentLoaded)
                    state.LoadContent(content);
            }

            contentLoaded = true;
        }

        public void UnloadContent()
        {
            foreach (IGameState state in states)
            {
                state.UnloadContent();
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (IGameState state in states)
            {
                state.Update(gameTime);
            }
        }

        public void Draw(GameTime gameTime)
        {
            foreach (IGameState state in states)
            {
                if (state.drawLayer > 0)
                    state.Draw(gameTime);
            }
        }
    }
}
