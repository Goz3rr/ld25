﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using LD25.World;
using LD25.Pandemic;
using LD25.Gui;

namespace LD25
{
    public class PlayState : IGameState
    {
        public int drawLayer { get; set; }
        public bool hasControl { get; set; }
        public bool contentLoaded { get; set; }

        PandemicManager pandemicManager;
        NewsTicker newsTicker;

        DefaultGenerator worldGenerator;
        Texture2D world;
        Texture2D roads;
        City hoverCity;

        Texture2D cityIcon, bioHazardIcon, deadIcon;

        Viewport GameView;

        bool win = false;
        float menuOffset = -16;

        public void Init()
        {
            GameView = Main.graphicsDevice.Viewport;
            GameView.Height -= 32;
            GameView.Y = 16;
        }

        public void LoadContent(ContentManager content)
        {
            newsTicker = new NewsTicker();
            pandemicManager = new PandemicManager(newsTicker);            

            worldGenerator = new DefaultGenerator(Main.graphicsDevice.Viewport.Width, Main.graphicsDevice.Viewport.Height-32);
            world = worldGenerator.GenerateImage();
            pandemicManager.cities = worldGenerator.GenerateCities();
            roads = worldGenerator.GenerateRoads(world, pandemicManager.cities);

            cityIcon = content.Load<Texture2D>("cityicon");
            bioHazardIcon = content.Load<Texture2D>("biohazard");
            deadIcon = content.Load<Texture2D>("dead");
            pandemicManager.PandemicSpread = content.Load<Texture2D>("airborne");

            contentLoaded = true;
        }

        public void UnloadContent()
        {
            
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState K = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            if (hasControl && Main.game.IsActive)
            {
                pandemicManager.Update(gameTime);
                newsTicker.Update(gameTime);

                if (pandemicManager.disease != null)
                {
                    hoverCity = null;
                    int alive = 0;
                    foreach (City city in pandemicManager.cities)
                    {
                        alive += (city.infectedpeople + city.alivepeople);
                        if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle((int)(city.pos.X - city.size / 2), (int)(city.pos.Y - city.size / 2), city.size, city.size)))
                        {
                            hoverCity = city;

                            if (pandemicManager.startCity == null && ms.LeftButton == ButtonState.Pressed)
                            {
                                newsTicker.addItem(new NewsItem("The pandemic started in " + city.name + "!", Color.Red));
                                pandemicManager.startCity = city;
                                pandemicManager.infectedCities.Add(city);
                                pandemicManager.totalInfected += 1;
                                city.infected = true;
                                city.infectedpeople = 1;
                                pandemicManager.objective = "Watch your pandemic spread and upgrade it!";
                                pandemicManager.paused = false;
                            }
                        }

                        if (city.transportOpen == true && city.infectedpeople > city.alivepeople)
                        {
                            city.transportOpen = false;
                            newsTicker.addItem(new NewsItem(city.name + " has closed it's borders!", Color.Red));
                        }
                    }

                    if (alive <= 0)
                    {
                        pandemicManager.paused = true;
                        pandemicManager.objective = "You won!";
                        win = true;
                    }

                    if (pandemicManager.totalInfected + pandemicManager.totalDeath > (alive * 0.9) && pandemicManager.disease.detected == false)
                    {
                        pandemicManager.disease.detected = true;
                        pandemicManager.disease.hygiene *= 1.25f;
                        newsTicker.addItem(new NewsItem(pandemicManager.disease.name + " has been detected!", Color.Red));                        
                    }                    
                }

                if (pandemicManager.currentGui == null && pandemicManager.startCity != null)
                {
                    if (menuOffset == 0)
                    {
                        if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(0, 64, 16, 256)))
                        {
                            menuOffset = MathHelper.Lerp(menuOffset, 176, 0.1f);
                        }
                    }
                    else if (menuOffset < 0)
                    {
                        menuOffset = MathHelper.Lerp(menuOffset, 0, 0.1f);

                        if (menuOffset > -0.1) menuOffset = 0;
                    }
                    else
                    {
                        if (Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(0, 64, 16 + (int)menuOffset, 256)))
                        {
                            menuOffset = MathHelper.Lerp(menuOffset, 176, 0.1f);
                        }
                        else
                        {
                            menuOffset = MathHelper.Lerp(menuOffset, 0, 0.1f);
                        }
                    }

                    if (ms.LeftButton == ButtonState.Pressed && Util.XYInsideRect(ms.X, ms.Y - 16, new Rectangle(-168 + (int)menuOffset, 272, 170, 40)))
                        pandemicManager.currentGui = new UpgradeDiseaseMenu(pandemicManager);
                }

                if (K.IsKeyDown(Keys.Space))
                {
                    world = worldGenerator.GenerateImage();
                    pandemicManager.cities = worldGenerator.GenerateCities();
                    roads = worldGenerator.GenerateRoads(world, pandemicManager.cities);
                }
                
            }
        }

        public void Draw(GameTime gameTime)
        {
            Viewport original = Main.graphicsDevice.Viewport;
            
            Main.spriteBatch.DrawString(Main.font, pandemicManager.getDate(), new Vector2(Main.graphicsDevice.Viewport.Width - Main.font.MeasureString(pandemicManager.getDate()).X - 4, 1), Color.White);
            Main.spriteBatch.DrawString(Main.font, "Current objective: " + pandemicManager.objective, new Vector2((int)Math.Floor(Main.graphicsDevice.Viewport.Width / 2 - Main.font.MeasureString("Current objective: " + pandemicManager.objective).X / 2), 1), Color.White);
            
            Main.spriteBatch.Draw(Main.whiteTexture, new Rectangle(0, 16, Main.graphicsDevice.Viewport.Width, 1), Color.LightGray);
            
            Main.graphicsDevice.Viewport = GameView;
            {
                Main.graphicsDevice.Clear(Color.Black);
                SpriteBatch spriteBatch = new SpriteBatch(Main.graphicsDevice);
                //spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone);
                spriteBatch.Begin();

                spriteBatch.Draw(world, new Rectangle(0, 0, Main.graphicsDevice.Viewport.Width, Main.graphicsDevice.Viewport.Height), Color.White);
                spriteBatch.Draw(roads, new Rectangle(0, 0, Main.graphicsDevice.Viewport.Width, Main.graphicsDevice.Viewport.Height), Color.White);

                SpriteBatch popUpBatch = new SpriteBatch(Main.graphicsDevice);
                popUpBatch.Begin();

                foreach (City city in pandemicManager.cities)
                {
                    if (city == hoverCity && pandemicManager.currentGui == null)
                    {
                        spriteBatch.Draw(cityIcon, new Rectangle((int)(city.pos.X - city.size / 2), (int)(city.pos.Y - city.size / 2), city.size, city.size), Color.Gray);

                        int offset = city.pos.X > Main.graphicsDevice.Viewport.Width - 144 ? -144 : 0;

                        popUpBatch.Draw(Main.whiteTexture, new Rectangle((int)(city.pos.X - city.size / 2) + city.size + 2 + offset, (int)(city.pos.Y - city.size / 2) - 16, 128, 48), Color.Gray);
                        popUpBatch.Draw(Main.whiteTexture, new Rectangle((int)(city.pos.X - city.size / 2) + city.size + 3 + offset, (int)(city.pos.Y - city.size / 2) - 15, 126, 46), Color.DarkGray);

                        Util.DrawShadowText(popUpBatch, Main.font, city.name, new Vector2((city.pos.X - city.size / 2) + city.size + 6 + offset, (int)(city.pos.Y - city.size / 2) - 16), Color.White);
                        Util.DrawShadowText(popUpBatch, Main.font, "Healthy: " + city.alivepeople, new Vector2((city.pos.X - city.size / 2) + city.size + 6 + offset, (int)(city.pos.Y - city.size / 2) - 6), Color.White);
                        Util.DrawShadowText(popUpBatch, Main.font, "Infected: " + city.infectedpeople, new Vector2((city.pos.X - city.size / 2) + city.size + 6 + offset, (int)(city.pos.Y - city.size / 2) + 4), Color.White);
                        Util.DrawShadowText(popUpBatch, Main.font, "Dead: " + city.deadpeople, new Vector2((city.pos.X - city.size / 2) + city.size + 6 + offset, (int)(city.pos.Y - city.size / 2) + 14), Color.White);
                    }
                    else
                        spriteBatch.Draw(cityIcon, new Rectangle((int)(city.pos.X - city.size / 2), (int)(city.pos.Y - city.size / 2), city.size, city.size), Color.Black);
                                        
                    if (city.alivepeople + city.infectedpeople <= 0)
                    {
                        spriteBatch.Draw(deadIcon, new Rectangle((int)(city.pos.X - city.size / 2) - city.size / 2, (int)(city.pos.Y - city.size / 2) - city.size / 2, 9, 14), Color.Black);
                        spriteBatch.Draw(deadIcon, new Rectangle((int)(city.pos.X - city.size / 2) - city.size / 2, (int)(city.pos.Y - city.size / 2) - city.size / 2, 8, 13), Color.DarkSlateGray);
                    }
                    else if (city.infected)
                    {
                        spriteBatch.Draw(bioHazardIcon, new Rectangle((int)(city.pos.X - city.size / 2) - city.size / 2, (int)(city.pos.Y - city.size / 2) - city.size / 2, 17, 17), Color.DarkGreen);
                        spriteBatch.Draw(bioHazardIcon, new Rectangle((int)(city.pos.X - city.size / 2) - city.size / 2, (int)(city.pos.Y - city.size / 2) - city.size / 2, 16, 16), Color.LawnGreen);
                    }
                }                

                pandemicManager.Draw(gameTime, spriteBatch);

                popUpBatch.Draw(Main.whiteTexture, new Rectangle(-176 + (int)menuOffset, 64, 192, 256), Color.Gray);
                popUpBatch.Draw(Main.whiteTexture, new Rectangle(-175 + (int)menuOffset, 65, 190, 254), Color.DarkGray);

                Util.DrawShadowText(popUpBatch, Main.font, "Score: " + pandemicManager.score, new Vector2(-168 + (int)menuOffset, 72), Color.White);
                Util.DrawShadowText(popUpBatch, Main.font, "Total deaths: " + pandemicManager.totalDeath, new Vector2(-168 + (int)menuOffset, 88), Color.White);
                Util.DrawShadowText(popUpBatch, Main.font, "Total infected: " + pandemicManager.totalInfected, new Vector2(-168 + (int)menuOffset, 104), Color.White);

                Util.DrawShadowText(popUpBatch, Main.font, "DNA Points: " + (int)pandemicManager.dnapoints, new Vector2(-168 + (int)menuOffset, 256), Color.White);
                popUpBatch.Draw(Main.whiteTexture, new Rectangle(-168 + (int)menuOffset, 272, 170, 40), Color.Gray);
                popUpBatch.DrawString(Main.font_big, "Upgrade", new Vector2(-152 + (int)menuOffset, 267), Color.Black);

                if (win == true)
                {
                    Util.DrawShadowText(spriteBatch, Main.font_big, "You won!", new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("You won!").X / 2, Main.graphicsDevice.Viewport.Height / 2), Color.White);
                    Util.DrawShadowText(spriteBatch, Main.font_big, "Score: " + pandemicManager.score, new Vector2(Main.graphicsDevice.Viewport.Width / 2 - Main.font_big.MeasureString("Score: " + pandemicManager.score).X / 2, Main.graphicsDevice.Viewport.Height / 2 + 32), Color.White);
                }

                spriteBatch.End();
                popUpBatch.End();
            }

            Main.graphicsDevice.Viewport = original;

            Main.spriteBatch.Draw(Main.whiteTexture, new Rectangle(0, Main.graphicsDevice.Viewport.Height-16, Main.graphicsDevice.Viewport.Width, 1), Color.LightGray);
            newsTicker.Draw(gameTime);  
        }
    }
}
